// Includes
const express = require('express')
const app = express();
const path = require('path');
const app_config_path = process.env.config_path || path.normalize(__dirname + '/config.json')
const config = require(app_config_path)
const cors = require('cors')
const bodyParser = require('body-parser')

// Handle app configuration
app.config = config
app.config.port = process.env.port || config.port || 5000
app.config.trips_path = process.env.trips_path || config.trips_path || path.normalize(__dirname + '/trips/')
app.config.client_url = process.env.client_url || config.client_url || 'http://localhost:3000'

// Configure CORS options
const corsOptions = {
    origin: app.config.client_url,
}
app.use(cors(corsOptions));

// Configure body parser
app.use(bodyParser.json())


// Set up server
var server = require('http').Server(app);

// Start server
server.listen(config.port, () => {
  // Load routes
  require('./routes')(app)
  console.log(`Comma Intern Server listening on port ${config.port}`)
});
