var fs = require('fs');
const path = require('path');
module.exports = function(app) {
  app.get('/api/get-trips', function(req, res) {
    const trips = fs.readdirSync(app.config.trips_path)
    console.log(`[INFO] Found ${trips.length} trips`)
    res.json(trips)
  })
}
