var fs = require('fs');
const path = require('path');
module.exports = function(app) {
  app.post('/api/get-trip', function(req, res) {
    // TODO: fix lfi vuln
    const data = JSON.parse(fs.readFileSync(path.normalize(app.config.trips_path + "/" + req.body.trip)));
    const coordinates = data.coords.map((coord) => {
      return ([
        coord.lng,
        coord.lat
      ])
    })
    const routePath = {
      "type": "Feature",
      "properties": {
        "name": data.start_time,
        "color": "#FF00FF"
      },
      "geometry": {
        "type": "MultiLineString",
        "coordinates": [coordinates]
      }
    }
    const speedPoints = data.coords.map((coord) => {
      return {
        "type": "Feature",
        "properties": {
          "name": data.start_time,
          "speed": coord.speed
        },
        "geometry": {
          "type": "Point",
          "coordinates": [coord.lng, coord.lat]
        }
      }

    })
    const features = [].concat(routePath).concat(speedPoints)
    console.log(`[INFO] Served trip ${data.start_time}`)
    res.json({
      "type": "FeatureCollection",
      features
    });
  });
}
