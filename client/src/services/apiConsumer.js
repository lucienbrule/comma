const API_URL =  window.appConfig.API_URL
function getTrip(trip){
  return makeRequest().then( (data)=> {
    return data;
  });
   function makeRequest(){
     return fetch(API_URL + "/api/get-trip", {
     method: "POST",
     body: JSON.stringify({"trip": trip}),
     headers: {
       "Content-Type": "application/json; charset=utf-8"
     }
   }).then((response) => {
     return response.json()
   }).catch((error) => {
     console.log(error)
   })
 }
}
function getTrips(){
  return makeRequest().then( (data)=> {
    return data;
  });
   function makeRequest(){
     return fetch(API_URL + "/api/get-trips", {
     method: "GET",
   }).then((response) => {
     return response.json()
   }).catch((error) => {
     console.log(error)
   })
 }
}

module.exports = {
  getTrips,
  getTrip
}
