import React from 'react';
import './App.css';
import Map from '../map/Map.js'
function App() {

  return (
    <div className="App">
    <header className="App-header">
      <img className="commaHeaderLogoImage" src="/comma-circle-black_32x32.png"/><span className="commaHeaderLogo">Comma Intern Server</span>
    </header>
      <Map />
    </div>
  );
}

export default App;
