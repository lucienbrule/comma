import React from 'react';
import {Component} from 'react';
import ReactMapGL, {Marker,StaticMap} from 'react-map-gl';
import DeckGL from '@deck.gl/react';
import {GeoJsonLayer} from '@deck.gl/layers';
import List from 'react-list-select'
import './Map.css';
import {getTrip,getTrips} from '../../services/apiConsumer'
import { ToastContainer,toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function determinePointColor(d){
  if(d === undefined) return [255,255,255]
  const speed = d.properties.speed;
  if(speed < 10){
    return [255,100,100,150]  // red
  }else if(speed < 15){
    return [255,90,30,150]    // orange ish
  }else if(speed < 20){
    return [255,255,0,150]    // yellow
  }else if(speed < 30){
    return [223,255,0,150]    // lime green ish (TIL: Charteuse)
  }else{
    return [100,200,100,150]  //green
  }
}
function determineLineColor(d){
  return [Math.random() * 255,Math.random() * 255,Math.random() * 255,100]
}

class Map extends Component {
  constructor(props){
    super(props);
    this.state = {
      viewport: {
        width: 100,
        height: 100,
        latitude: 37.7577,
        longitude: -122.4376,
        zoom: 8
      },
      initialViewState:{
        latitude: 37.7577,
        longitude: -122.4376,
        zoom: 8
      },
      availableTrips: [],
      selectedTrips: [],
      selected:[],
      buttonText: "DRAW ALL",
      tripDataArray: []

    };
    this.mapContainer = React.createRef();
    this.generateTripDataArray = this.generateTripDataArray.bind(this)
    this.handleTripSelect = this.handleTripSelect.bind(this)
    this.handleDrawAll = this.handleDrawAll.bind(this)

  }
   componentWillMount(){
     getTrips().then( (availableTrips) => {
       this.setState({
         availableTrips
       })
     })
   }
   componentDidMount(){
     toast("Click a date to select a route")
   }
   handleTripSelect(selected){
     const newSelected = selected;
     const newSelectedTrips = selected.map( (index) => {
       return this.state.availableTrips[index];
     })
    if(this.state.buttonText == "DRAW ALL"){
      this.generateTripDataArray(newSelectedTrips).then( (newTripDataArray)=>{
        this.setState({
          selected:  newSelected,
          selectedTrips: newSelectedTrips,
          tripDataArray: newTripDataArray
        })
      })
    }
   }
   handleDrawAll(){
     const toastId = toast("Loading all the routes, drink some coffee. ☕️")
     const newSelected = this.state.availableTrips.map( (d,i) => {return i})
     const newSelectedTrips = this.state.availableTrips
    if(this.state.buttonText == "DRAW ALL"){
      this.generateTripDataArray(newSelectedTrips,toastId).then( (newTripDataArray)=>{
        toast.done(toastId)
        this.setState({
          selected:  newSelected,
          selectedTrips: newSelectedTrips,
          tripDataArray: newTripDataArray
        })
      })
    }
   }
  generateTripDataArray(selectedTrips){
    var tripDataArrayPromise = [];
    if(selectedTrips !== undefined && selectedTrips.length > 0){
      var progress = 0;
      tripDataArrayPromise = selectedTrips.map( (tripName,index) =>{
        return getTrip(tripName)
      })
    }
    return Promise.all(tripDataArrayPromise)
  }
  componentWillUpdate(nextProps,nextState){

  }
  render(){
    return (
      <div className="Map">
        <div className="controlContainer">
          <input type="button"
            onClick={this.handleDrawAll}
            value={this.state.buttonText}
            className="ghostButton"
            />
          <List
            className="avalibleTripList"
            items={this.state.availableTrips}
            selected={this.state.selected}
            multiple={true}
            onChange={ this.handleTripSelect }
          />
        </div>
        <div className="deckContainer">
          <DeckGL
            initialViewState={this.state.initialViewState}
            controller={true}
            >
          <StaticMap
            mapboxApiAccessToken={window.appConfig.mapboxApiAccessToken}
          />
        {this.state.tripDataArray.map((tripData)=>{
          return(
            <GeoJsonLayer
              key={Math.random()}
              data={tripData}
              pickable= {false}
              stroked={false}
              filled= {true}
              extruded={true}
              lineWidthScale={1}
              lineWidthMinPixels= {2}
              pointRadiusMinPixels= {2}
              getFillColor={d=>determinePointColor(d)}
              getRadius= {3}
              getLineWidth= {1}
              getElevation= {30}
              getLineColor= {d => determineLineColor(d)}
             />
          )
        })}
          </DeckGL>
        </div>
        <ToastContainer
          className='toastContainer'
          toastClassName="darkToast"
        />
      </div>
    );

  }
}

export default Map;
