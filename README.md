

# Assignment

> Below are trips from our dash cam users in the SF Bay Area, each of which is a JSON file containing an array of coordinates and speed (m/s) sampled at once per second. Build a performant web app containing an interactive map visualizing the distribution of speeds among these trips. All trips should be simultaneously visible on the map.

# Implementation

## Client

I used Facebook's create-react-app to generate the boilerplate then used the eject functionality to get the build scripts.

The client consume's the server's API endpoints, get-trips and get-trip then displays the data on a Mapbox map.

For the map I used Uber's deck.gl library to do the visualization. This makes it straightforward to render GeoJSON formatted data to the map.  

## Server

I used NodeJS and the express framework. The server has two api endpoints and a health endpoint.

In lieu of using a database, I opted to just parse files from disk. There is a lot of room for performance optimization if a database was used, but for the limited scope of this project I found it would take more time than necessary.

```/api/get-trips``` gets a listing of any trips in the trips folder.

 There is a local file inclusion vulnerability left in the code. This is left as an exercise to the reader, in the meantime it serves as a feature.

 ```/api/get-trip``` parses a trip by file name and formats it to fit the GeoJSON spec.

 ## Build Pipeline

 The build pipeline uses Docker and Jenkins. This would be the theoretical first step in setting up a CI/CD pipeline. I found it convenient to use Jenkins and Docker over other platforms like Heroku or Firebase because I readily have access to a box running Jenkins.

 Pretty what happens is I have a git hook setup on my BitBucket account that calls back to my Jenkins server, the server pulls the code and builds it (as detailed in the Jenkins file) then deploys it by running the docker-compose file.

 # Outcome

 This was probably the most fun interview assignment I've ever been assigned. Much better than a code golf-esque challenge. One of the requirements was the application be performant. I'd say there a litte bit of lag on loading all of the trips at once. If I had more time to spend I'd figure out a way to load the routes incrementally, to give some intermediate feedback to the user. I did some cursory profiling and most of the latency on requests comes from parsing JSON. If I had setup say MongoDB and preloaded all of the routes on startup much of the latency would probably go away. I could have also gone the route of pre transforming all of the files during first run of the server into GeoJSON.

 # Anyway,

 Thank you for considering my application and I hope you enjoy my submission.

 ---

 # Development

1. ```yarn install``` on both client and server folders
2. ```yarn start``` on both client and server folders
3. !???
4. open localhost:3000

# Production

configuration is provided for an environment with docker, Traefik, docker-compose and Jenkins.
If you have an environment like mine and want to host this on your own domain all you *should* have to do is change the comma.<your_domain> urls in the configuration. The Dockerfile Args can be overwritten by the docker-compose file. Youll also have to create a corresponding docker netowrk.
